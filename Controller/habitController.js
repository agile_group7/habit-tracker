const Habit = require('../Model/habitModel')
const AppError = require('./../utils/appError')

exports.addHabit=(req,res,next)=>{
    try{
        const newHabit=new Habit.create(req.body)
        res.status(201).json({
            status:'success',
            data:newHabit
        })
    }catch(e){
        console.error(e); // Log the error
        res.status(500).json({
            error: e.message,
        });
    }
}

exports.deleteHabit=(req, res) => {
    try {
        const {id} = req.params
        Habit.findByIdAndDelete(id)
      .then(habit => {
            res.status(200).json({
                status:'success',
                data: habit
            })
        })
      .catch(err => {
            res.status(500).json({
                error: err.message
            })
        })
    } catch (e) {
        res.status(500).json({
            error: e.message
        })
    }
}

exports.getHabitNames=(req, res) => {
    try {
        Habit.find()
      .then(habit => {
            res.status(200).json({
                status:'success',
                data: habit
            })
        })
      .catch(err => {
            res.status(500).json({
                error: err.message
            })
        })
    } catch (e) {
        res.status(500).json({
            error: e.message
        })
    }
}