const User = require('./../Model/userModel')

exports.getAllUsers = async(req, res, next) => {
    try {
        const users = await User.find()
        res.status(200).json({data: users, status:'success'})
    } catch (e){
        res.status(500).json({error: e.message});
    }
}

exports.createUser = async (req, res) => {
    try {
        const user = await User.create(req.body);
        console.log(req.body.name)
        res.status(200).json({data: user, status:'success'})
    } catch (e) {
        res.status(500).json({error: e.message});
    }
}

exports.getUser = async (req, res) => {
    try {
        const user = await User.findById(req.params.id);
        res.status(200).json({data: user, status:'success'})
    } catch (e) {
        res.status(500).json({error: e.message});
    }
}

exports.updateUser = async (req, res) => {
    try {
        const user = await User.findByIdAndUpdate(req.params.id, req.body);
        res.status(200).json({data: user, status:'success'})
    } catch (e) {
        res.status(500).json({error: e.message});
    }
}

exports.deleteUser = async (req, res) => {
    try {
        const user = await User.findByIdAndDelete(req.params.id);
        res.status(200).json({data: user, status:'success'})
    } catch (e) {
        res.status(500).json({error: e.message});
    }
}