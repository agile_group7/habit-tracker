const path = require('path')

//get landingpage
exports.getLandingPage = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'LandingPage.html'))
}

//login page
exports.getLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'Login.html'))
}

//singup page
exports.getSignupForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'signup.html'))
}

exports.getHome=(req,res)=>{
    res.sendFile(path.join(__dirname,'../','views','HomePage.html'))
}

exports.getConfirmationOTP=(req,res)=>{
    res.sendFile(path.join(__dirname,'../','views','ConfirmationPage.html'))
}
exports.getRegistrationSuccess=(req,res)=>{
    res.sendFile(path.join(__dirname,'../','views','SuccessfullRegister.html'))
}

exports.getAddTask=(req,res)=>{
    res.sendFile(path.join(__dirname,'../','views','AddTask.html'))
}

exports.getTaskCalander=(req,res)=>{
    res.sendFile(path.join(__dirname,'../','views','TaskCalander.html'))
}
exports.getProfile=(req,res)=>{
    res.sendFile(path.join(__dirname,'../','views','Profile.html'))
}




