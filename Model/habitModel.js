const mongoose = require('mongoose')


const habitSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true,'Name your habit!'],
    },
    reward :{
        type: String,
        required: [true,'Define your reward']
    },

    duration : {
        type : Number,
        required:[true, 'Set the duration']
    }
})




const Habit = mongoose.model('Habit',habitSchema)
module.exports = Habit;