const express = require("express")
const path = require('path'); // Import the 'path' module

const userRouter = require('./routes/userRoutes')
const viewRouter = require('./routes/viewRoutes')
const habitRouter=require('./routes/habitRoutes')

const app = express()
app.use(express.json())
app.use('/api/v1/users', userRouter)
app.use('/api/v1/habit', habitRouter)

app.use(express.static(path.join(__dirname, 'views')))
app.use('/', viewRouter)
module.exports = app
