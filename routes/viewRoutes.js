const express = require('express')
const router = express.Router()
const viewsController = require('./../Controller/viewController')



router.get('/', viewsController.getLandingPage)
router.get('/login',viewsController.getLoginForm)
router.get('/signup', viewsController.getSignupForm)
router.get('/home', viewsController.getHome)
router.get('/confirmationOTP',viewsController.getConfirmationOTP)
router.get('/registrationSuccess',viewsController.getRegistrationSuccess)
router.get('/addtask',viewsController.getAddTask)
router.get('/taskcalender',viewsController.getTaskCalander)
router.get('/profile',viewsController.getProfile)

module.exports = router
