// const { addHabit } = require("../../Controller/habitController")

const addHabit = async(habit,duration,reward) => {
    try{
        const res = await axios({
            method: 'post',
            url: 'http://localhost:4001/api/v1/habit/addhabit',
            data: {
              habit,
              duration,
              reward
            },
        })

        console.log(res)
        if (res.data.status === 'success') {
            window.setTimeout(() => {
                location.assign('/taskcalender')
            },1500)

        }
    }catch (err) {
        let message=
        typeof err.response !== 'undefined'
        ? err.response.data.message
        : err.message
       console.log(message)
    }
}
document.querySelector('.form').addEventListener('submit',(e)=>{
    e.preventDefault()
    const habit=document.getElementById('habit').value 
    const duration = document.getElementById('duration').value
    const reward=document.getElementById('reward').value
    addHabit(habit,duration,reward)
})