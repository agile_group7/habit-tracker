// Sample user data (you can replace this with your data)
const users = [
    { name: "John Doe", email: "john@example.com", status: "Active" },
    { name: "Jane Smith", email: "jane@example.com", status: "Inactive" },
    // Add more user data here
];

// Function to populate the table
function populateTable() {
    const tbody = document.querySelector("tbody");
    tbody.innerHTML = ''; // Clear the table before repopulating

    users.forEach((user, index) => {
        const row = document.createElement("tr");
        row.innerHTML = `
            <td>${index + 1}</td>
            <td>${user.name}</td>
            <td>${user.email}</td>
            <td>${user.status}</td>
            <td class="delete-icon" data-index="${index}">&#x2716;</td>
        `;

        tbody.appendChild(row);
    });
}

// Function to display a confirmation modal
function showConfirmationModal(index) {
    const modal = document.createElement("div");
    modal.className = "modal";
    modal.innerHTML = `
        <div class="modal-content">
            <h2>Delete</h2>
            <p>Are you sure you want to delete?</p>
            <div class="modal-buttons">
                <button id="confirm">Confirm</button>
                <button id="cancel">Cancel</button>
            </div>
        </div>
    `;

    // Add event listeners for Confirm and Cancel buttons
    const confirmButton = modal.querySelector("#confirm");
    const cancelButton = modal.querySelector("#cancel");

    confirmButton.addEventListener("click", () => {
        // Get the user index from the data-index attribute
        const userIndex = parseInt(index);

        // Ensure the user index is valid
        if (!isNaN(userIndex) && userIndex >= 0 && userIndex < users.length) {
            // Remove the user from the users array
            users.splice(userIndex, 1);
            populateTable();
        }

        closeConfirmationModal();
    });

    cancelButton.addEventListener("click", () => {
        closeConfirmationModal();
    });

    document.body.appendChild(modal);
}

// Function to close the confirmation modal
function closeConfirmationModal() {
    const modal = document.querySelector(".modal");
    if (modal) {
        modal.remove();
    }
}

// Add click event for delete icons
document.addEventListener("click", (event) => {
    if (event.target.classList.contains("delete-icon")) {
        const index = event.target.getAttribute("data-index");
        showConfirmationModal(index);
    }
});

// Call the populateTable function when the page loads
populateTable();
