import { showAlert } from "./alert.js";

export const addHabit= async (name,reward,duration) => {
    try{
        const res=await axios({
            method:'POST',
            url:'http://localhost:4001/api/v1/habit/addHabit',
            data:{
                name,
                reward,
                duration
               
            }
        })
        if(res.data.status==='success'){
            showAlert('success','Habit added successsfully!')
            window.setTimeout(() => {
                location.assign('/taskcalender')
                
            }, 1500)
        }
    }catch(err) {
        res.status(500).json({
            error:err.message
        })
    }
}


export const getHabitNames= async()=>{
    try{
        const res=await axios({
            method:'GET',
            url:'http://localhost:4001/api/v1/habit/getHabitNames'
        })
        return res.data;
    }catch(err) { 
        res.status(500).json({
            error:err.message
        })

    }
    
}

document.querySelector('.form').addEventListener('submit', (e) =>{
    e.preventDefault()
    const name = document.getElementById('name').value
    const reward = document.getElementById('reward').value
    const duration = document.getElementById('duration').value
    addHabit(name,reward,duration)
})
