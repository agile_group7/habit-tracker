import { showAlert } from "./alert.js";

export const signup = async (name, email, password, passwordConfirm) => {
    try {
        const res = await axios({
            method: 'POST',
            url: 'http://localhost:4001/api/v1/users/signup',
            data: {
                name,
                email,
                password,
                passwordConfirm
            },
        })
        if (res.data.status === 'success') {
            showAlert('success', 'Account created successsfully!')
            window.setTimeout(() => {
                location.assign('/login')

            }, 1500)
        }
    } catch (err) {
        let message = 
        typeof err.response !== 'undefined'
            ? err.response.data.message
            :err.message
        showAlert('error', 'Error: passwords are not the same!', message)
        console.log(message);
        console.log(err);
    }
}

document.querySelector('.form').addEventListener('submit', (e) =>{
    e.preventDefault()
    const name = document.getElementById('username').value
    const email = document.getElementById('email').value
    const password = document.getElementById('password').value
    const passwordConfirm = document.getElementById('confirm_password').value
    signup(name, email, password,passwordConfirm)
})